---
layout: handbook-page-toc
title: "Sourcing"
description: "Sourcing has proved itself to be a great channel for attracting the best talent, this page details how we source talent at GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose of Sourcing

The speed with which we can grow our team is dependent on the rate at which we find qualified candidates. We believe that embedding strategic sourcing in our Talent Acquisition practice allows us to not only provide an accurate snapshot of the total addressable market before we make a hire, but also, it is our opportunity to be intentional around building representation into the top of our funnel. It allows us to grow GitLab and build an even more diverse team in the most efficient way possible, while also sustaining our culture.

## Our Candidate Sources
* **Applications** - a person who has actively applied to one of our open roles through a job advertisement/posting or an event.
* **Referrals** - a person has been referred by a current GitLab team member.
* **Sourced (aka Passive)** - a person who is found during sourcing (searching) efforts by the TA team with or without the help of the hiring team (Source-a-thon).
This person is of interest, but has not interacted with the Talent Acquisition (TA) team or Hiring Manger yet.

## Prospects vs. Candidates
* **Prospect** - a person is qualified for a potential future or current role at GitLab, but has not yet formally engaged with moving forward to enter the interview process. There is basic information available (LinkedIn, Blogs, Conference Talks, etc.) and are housed in our CRM talent community.
* **Candidate** - a person that is interested in an active position at GitLab and is engaged in the interview process (which starts with a recruiter phone screen). These individuals are then tracked in our Applicant Tracking System to engage through the interview process. 

## How to Source Candidates
### For Talent Acquisition Team Members
At GitLab, we consider each of our recruiter team members on the Talent Acquisition team 'full life cycle' which means, sourcing is a skill and part of the craft in what we do. In order to ensure we are able to bring a snap shot of the total addressable market for each role that we hire against, cultivating a passive pipeline of talent is an essential part of that process. It is also our opportunity to get very intentional when it comes to building representation into our pipelines. 

In accordance with our [values](/company/culture/inclusion/#values) of Diversity, Inclusion and Belonging, and to build teams as diverse as our users, Talent Acquisition provides a collection of sourcing tools [here](https://docs.google.com/spreadsheets/d/1Hs3UVEpgYOJgvV8Nlyb0Cl5P6_8IlAlxeLQeXz64d8Y/edit#gid=2017610662) to expand our hiring teams’ candidate pipelines. If you have any questions on how to implement these resources into your current sourcing strategy or have suggestions for new tools, please reach out to the Talent Acquisition team.

### Our Sourcing Tools
#### LinkedIn Talent Insights

Our Talent Acquisition team has two seats for LinkedIn's **Talent Insights** product. **Talent Insights** is a platform that is specialized in answering business questions regarding workforce planning, talent pools, competitive hiring analysis, and more from its database that exceeds 705 million members, 50 million companies, 20 million jobs, and 90,000 schools. The use of this tool is helpful in determining the diversity of a talent pool  (LinkedIn's best guess based on provided profile information), regional talent availability, and competitor analysis. To request such insights, please submit an Issue [here](https://gitlab.com/gl-talent-acquisition/operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) using the `LinkedIn Talent Insights Request` template.

Once a report has been run, it can be exported in either a `.PDF` or `.CSV` format; the latter allowing you to filter and segment the data. All reports will be uploaded to the [LinkedIn Talent Insights Reports](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/tree/master/LinkedIn%20Talent%20Insights%20Reports/Talent%20Insights%20Reports) project. If there's a report in that project that you'd like refreshed, please submit a new request Issue.

Our Executive Recruitment team performs research before the start of every Dir+ role which leverages our LI Talent Insights tool. The use throughout the rest of the team is discretionary based on need and sourcing strategy. 

#### SeekOut
Each quarter, we allocate SeekOut seats strategically through our recruitment team based on need (volume of roles and alignment to goals), usage and opportunity to drive impact to priority pipelines. Allocations are established during the first two weeks of the quarter. If you are interested in having a seat allocated for the following quarter, be sure to raise the request with your manager for consideration. 

#### LinkedIn Sourcing

[View LinkedIn Training slide](https://docs.google.com/presentation/d/1W9PvVp2uGFsWHFTQrAd5ZjgzfAMmS_dQI6R7Lg7XDJc/edit#slide=id.g7ba34d75e8_0_16) to understand the Boolean logic. 

#### Greenhouse Sourcing

1. Candidates -> Application Type -> Candidates + Prospects only -> now exclude active people (so you can see also those who haven't been checked yet)
2. Filter by job  -> All reqs
3. Search with specific keywords/skills in the top left search bar. Turn on the "Full Text Search" function to get more results.
4. Search using tags as filters. Profile details -> Candidate Tags
5. Here is the [Video](https://drive.google.com/file/d/1pojTfH5S08T_1mMbbGT682URRO_nW4tm/view?usp=sharing) (internal team members only)!

**How to source our Greenhouse database through LinkedIn**

Simply add all your filters as described above in the LinkedIn Recruiter sourcing section. After setting up your search parameters, add one extra: “In ATS” - here add Greenhouse. This will show you all the people who match your search and are in our database and also have a LinkedIn profile. 

**Bonus Greenhouse sourcing tip**

You can download several profiles in bulk in an XLS file and you can run your searches in the spreadsheet. For this, click on the blue “Export” button on the top of your search results in the “All candidates” section of a search result tab.

#### Pitch Pages 

Pitch Pages are a [Guide](https://about.gitlab.com/handbook/hiring/interviewing/#guide)-like experience, intended for prospective candidates that have yet to dive into learning about the experience of working at GitLab, or for current candidates who are in the process and interested in learning more. Pitch Pages are a great sourcing tool aimed to help convert a prospect into a true candidate, or to provide more insight about our company for those currently in the process.

To help form a strong and meaningful relationship with both passive and active candidates, Recruiters can leverage a unique Pitch Page built specific to their departments including Sales, Engineering, UX, G&A, Product, or our General Pitch Page. Pitch Pages will highlight GitLab’s achievements, life at GitLab, our values, culture, benefits, and working on a specific team! Recruiters may choose to send the Pitch Page via Inmails, LinkedIn messages or use it in any strategy for sourcing by sharing this [link](https://app.guide.co/p/c462b99a-d4a6-4246-9e5c-e4232c7bd958) with candidates.

The Candidate Experience Team will be the DRI for Pitch Pages. If you have any questions or suggestions please reach out to CES at ces@gitlab.com.


### Re-engagement 
The recommended process of re-contacting candidates:

1. Re-contacting a candidate for the same/another position if the candidate failed in the previous interview process: we suggest other TA member re-contact them after 6+ months from the time of their last interview. Please consult with the other TA member who contacted/recruited them previously and discuss the case, if needed.
2. Re-contacting the candidate for the same/another position if they haven't responded to the past inmails: the recommended time to contact the candidate again is around 3 months. Please consult with the other TA member who contacted them previously and discuss the case, if needed.

### For GitLab Team Members
If you are a Hiring Manager on a open role, your recruiter will partner with you to ensure you have a good action plan for building a passive candidate pool for your role.

Sourcing vs. Referrals: If you **know** a candidate from school, previous companies, seminars... even among your neighbors or family members - the best next step is to submit them as a referral by creating an `Issue` in the [Referrals Project](https://gitlab.com/gl-talent-acquisition/referrals/-/issues/new?issuable_template=Referral%20Submission). More information about the **Referrals** can be found on the [Referral Process](/handbook/hiring/referral-process/) page.

For anyone that you **do not** have a personal relationship with, please add them as a *Prospect* in Greenhouse. Talent Acquisition will be able to engage prospects during the interview process by leveraging information in Greenhouse.  

Here's how to do that:

1. Check if the prospect/candidate already exists in our ATS (Applicant Tracking System) by searching for the candidate's name in Greenhouse. Pay attention to the activity history both in Greenhouse (in Activity Feed), LinkedIn (Messages), or any other professional networking sites.
2. Click the **"Add"** button, then click **Add a Candidate**
3. Switch to the **Prospect** tab and complete the intake form
4. **After** receiving confirmation from a *Prospect* about moving forward with a vacancy, you can convert them into a *Candidate*

**Step by Step Example**

I've found someone who might be suitable for our Backend Engineer role, simply use their name or email address to check in Greenhouse to see their status
--> Add them as a *Prospect* in Greenhouse --> Once they replied with interests, please convert them into a *Candidate* in Greenhouse.
You can also check out our [Greenhouse Tips and Tricks](https://docs.google.com/document/d/1BbO5v_IJEq4QR9KpI7T3fSCwdCapVOZCyNgEk6MYO0s/) document to find more information on Greenhouse usage.

**About Sourcing Channels**

The most efficient way to source candidates for any vacancy is to search through a professional network, such as LinkedIn, AngelList, etc.
Professional networks make it easy to scan a person's skill set and professional background quickly and efficiently and are designed to present their best impression to potential employers.

We encourage our team members to think out of the box and source creatively! For some positions, other networks may prove useful as well -
for example, we are looking for someone who has public speaking experience combined with specific tech expertise.
You can go on YouTube and search for candidates who have spoken at seminars or professional conferences, try to search for the person's name and look for suitable ways to contact them.

**About Reaching Out to Candidates**

When you have identified someone as a good potential candidate, send their
profile along with any requested information to the TA member so they can reach out to
the candidate and add them to Greenhouse. You can check the Talent Acquisition alignment
[here](/handbook/hiring/recruiting-alignment/).

If you want to reach out to a sourced candidate directly, you should discuss your communication strategy with your
recruiting partner beforehand in order to avoid duplication and/or poor candidate experience.

Take a look at the [Content library - life at GitLab](https://about.gitlab.com/handbook/people-group/employment-branding/content-library/) for the latest team member count, employer awards, blog posts, and more facts about working at GitLab. These resources are meant to help guide your outreach to potential candidates.

**Upgrading Your LinkedIn Account to Hiring Manager**

Hiring Managers and hiring team members are able to request a **LinkedIn Hiring Manager Seat.** To upgrade your seat, please add your GitLab email to your LinkedIn profile by following this process: {+ Login to Linkedin > click Me > Settings & Privacy > Sign in & security > Email addresses > add your GitLab email address and verify it. +} Next, submit an [Access Request Issue](https://gitlab.com/gl-talent-acquisition/operations/issues/new?issuable_template=LinkedIn%20Access%20Request) using the LinkedIn Access Request template within the [Technology and Insights project](https://gitlab.com/gl-talent-acquisition/operations). 

- **A Hiring Manager seat** allows a user to collaborate on projects, where they can share and review profiles and provide feedback on prospective candidates.

**New to LinkedIn?** Please reach out to your TA partner for tips.

Please note, we won't be able to reimburse any LinkedIn seats purchased at your own expense.

If you're locked out of your LinkedIn account with no way to confirm your identification via 2 Step Verification, you are able to turn off 2 Step Verification.